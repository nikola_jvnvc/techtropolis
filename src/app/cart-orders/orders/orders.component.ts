import { Component, OnInit } from '@angular/core';
import { OrdersService, OrdersStatus } from 'src/app/services/orders.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Products } from 'src/app/services/products.service.service';
import { MatDialog } from '@angular/material/dialog';
import { UpdateOrderComponent } from '../update-order/update-order.component';

export interface ItemsOrder {
  image: string;
  name: string;
  price: any;
  quantity: any;
}

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  dialogOpen: boolean = false;

  orderSource: any = new MatTableDataSource<ItemsOrder>();
  displayedColumns = ["image", "name", "quantity", "price"];

  constructor(private ordersService: OrdersService, private productsService: Products, private _snackBar: MatSnackBar, private dialog: MatDialog) { }

  ngOnInit(): void {
    console.log("INIT ORDERS");
    this.orderSource.data = this.findAllByUsername(localStorage.getItem("username"));
    console.log("ORDERS: ",this.orderSource.data);
  }

  public findAllByUsername(username: string){
    return this.ordersService.findAllOrdersByUsername(username)
  }

  completeOrder(id: any){

    this.changeStatusComplete(id);
    this._snackBar.open("Narudžbina završena. Možete oceniti naružbinu na vašem profilu.","",{duration: 5000});
    this.orderSource.data = this.findAllByUsername(localStorage.getItem("username"));

  }

  cancelOrder(id: any, items:any){

    this.changeStatusCanceled(id);
    this._snackBar.open("Narudžbina je otkazana!","",{duration: 3000});
    this.orderSource.data = this.findAllByUsername(localStorage.getItem("username"));

    var itemArray = [];
    items.forEach(item => {
      itemArray.push({id: item.id, quantity: item.quantity})
    }); 

    this.cancelOrderBackQuantity(itemArray);
  }

  updateOrder(id: any, city: string, address: string, payment: string){
    this.dialogOpen = true;

        const orderDialog = this.dialog.open(UpdateOrderComponent, {
          disableClose: true,
          width: "60vw",
          data: { orderId: id, city:city, address:address, payment: payment }
        });

        orderDialog.afterClosed().subscribe(result => {
          this.dialogOpen = false;
          this.orderSource.data = this.findAllByUsername(localStorage.getItem("username"));
        })
  }

  public changeStatusComplete(id){
    var model: OrdersStatus = {
      "id": id,
      "status": "completed"
    }
     return this.ordersService.changeStatus(model);
  }

  public changeStatusCanceled(id){
    var model: OrdersStatus = {
      "id": id,
      "status": "canceled"
    }
     return this.ordersService.changeStatus(model);
  }

  public cancelOrderBackQuantity(items: any){
    this.productsService.cancelOrderBackQuantity(items).subscribe(value => {});
  }

  public mapStatus(status : string) : string {
    switch(status){
      case "pending" : return "Na čekanju";
      case "canceled" : return "Otkazano";
      case "completed" : return "Završeno";
      default : return status;
    }
  }

}
