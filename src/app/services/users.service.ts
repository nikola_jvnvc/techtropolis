import { Injectable } from '@angular/core';

export interface Users {
  fullname: string;
  email: string;
  username: string;
  password: string;
  city: string;
  address: string;
  birthday: any;
  picture: any;
}

export interface UsersLogin {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})



export class UsersService {

  private users : Users [] = [
    {
      fullname: "Test", email: "test@gmail.com", username: "test", password: "testtest", city: "Test1",
      address: "Test1", birthday: "Wed Jul 21 00:00:00 CEST 1999", picture: "../../assets/default-avatar.png"
    },
    {
      fullname: "Tesko Teskovic", email: "teskovic@gmail.com", username: "tesko", password: "test1234", city: "Beograd",
      address: "Topolska 18", birthday: "Wed Jul 21 13:00:00 CEST 1993", picture: "../../assets/default-avatar.png"
    }
  ];

  public insert(model: Users){
    return this.users.push(model);
  }

  public login(model: UsersLogin){
    let user : Users = this.users.find(user => user.username == model.username && user.password == model.password);
    return user ? user : null;
  }

  public findByUsername(username: String){
    let user : Users = this.users.find(user => user.username == username);
    return user ? user : null;
  }

  public update(model: Users){
    let user : Users = this.users.find(user => user.username == model.username);
    return user ? user : null;
  }

  public updatePicture(username: string, picUrl: string){
    let user : Users = this.users.find(user => user.username == username);
    if(user)
      user.picture = picUrl;
  }

}
