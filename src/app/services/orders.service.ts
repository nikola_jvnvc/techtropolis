import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users,UsersService } from './users.service';

export interface Orders {
  id?: number;
  username: string;
  city?: string;
  address?: string;
  payment: string;
  price: any;
  items: any;
  orderedAt: any;
  status: any;
}

export interface UpdateOrder {
  id: number;
  city: string;
  address:string;
  payment: string;
}

export interface OrdersStatus {
  id: any;
  status: any;
}

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private orderIncId = 0;

  private orders : Orders [] = [

  ];

  constructor(private usersService: UsersService) {};

  public findAllOrdersByUsername(username: String){

    return this.orders.filter(order => order.username == username && order.status == "pending");
  }

  public findAllOrdersHistoryByUsername(username: String){
    return this.orders.filter(order => order.username == username
       && (order.status == "completed" || order.status == "canceled"));
  }

  public insert(model: Orders){
    model.id = this.orderIncId++;
    let user : Users = this.usersService.findByUsername(model.username);
    if(user){
      model.address = user.address;
      model.city = user.city;
      console.log("ORDER INSERT:", model);
      this.orders.push(model);
      return model;
    }
    console.log("ORDER INSERT null user:", model)
    return null;
  }

  public changeStatus(model: OrdersStatus){
    this.orders.filter(order => order.id == model.id).map(order => order.status = model.status);
  }

  public update(model: UpdateOrder){
    let order : Orders = this.orders.find(order => order.id == model.id);
    if(order){
      order.address = model.address;
      order.city = model.city;
      order.payment = model.payment;
      console.log("UPDATE ORDER: ", order);
      return order;
    } else {
      console.log("UPDATE ORDER: ", order);
      return null;
    }
  }

  public deleteById(id: number){
    return this.orders.filter(order => order.id == id).pop();
  }

}
