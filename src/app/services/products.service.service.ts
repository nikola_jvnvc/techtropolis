import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export interface Item {
  id?: string;
  name: string;
  category: string;
  price: any;
  description: string;
  image: string;
  stars: any;
  rating: number;
  dateCreated: any;
  isActive: string;
  madeIn: string;
  quantity: number;
}

export interface QuantityDeleteCart {
  id: string;
  quantity: number;
  isActive: string;
}

export interface UpdatingStars {
  id: string;
  rating: number;
}

export interface Comment {
  itemId: string;
  username: string;
  content: string;
  postedAt: Date;
}

@Injectable({
  providedIn: 'root'
})
export class Products{

  private commentId : number = 2;

  private items : Item [] = [
    { id: "60be02d2f153960dd788f2a9", name: "ASUS RTX 3060 Dual OC", category: "gpu", price: 89999.99, 
    description: "GPU:Nvidia GeForce RTX 3060\nKoličina memorije:12GB\nTip memorije:GDDR6\nMagistrala memorije:192bit",
    image: "https://img.gigatron.rs/img/products/large/image611fb2f01dc3b.png",
    stars: [4,5,5], rating: 5, dateCreated: "2021-04-11T22:00:00", isActive: "Da", madeIn: "China", quantity: 10
    },
    { id: "60be02d2f153960dd788f2a0", name: "Gigabyte RTX 2060 6GB", category: "gpu", price: 74999.9, 
    description: "GPU:Nvidia GeForce RTX 2060\nKoličina memorije:6GB\nTip memorije:GDDR6\nMagistrala memorije:192bit",
    image: "https://img.gigatron.rs/img/products/large/GIGABYTE-GeForce-RTX-2060-D6-6GB-GDDR6-192-bit-(rev.-2.0)-GV-N2060D6-6GD-83.png",
    stars: [4], rating: 4, dateCreated: "2021-04-11 22:00:00", isActive: "Da", madeIn: "Taiwan", quantity: 14
    },
    { id: "be02d2f153960dd788f2a3", name: "INTEL Core i9-10920X", category: "cpu", price: 84999.9, 
    description: "Gaming:Da\nPodnožje:Intel® 2066\nTip procesora:Intel® Core™ i9\nBroj jezgara:12\nThreads:24\nTehnologija izrade:14 nm\nTDP:165W",
    image: "https://img.gigatron.rs/img/products/large/image5ef5da80ed54d.png",
    stars: [5], rating: 5, dateCreated: "2021-03-11 22:00:00", isActive: "Da", madeIn: "Taiwan", quantity: 20
    },
    { id: "aad2f153960dd788f2a4", name: "AMD Ryzen 7 3700X", category: "cpu", price: 74999.9, 
    description: "Gaming:Da\nPodnožje:AMD® AM4\nTip procesora:AMD® Ryzen 7\nBroj jezgara:8\n\rThreads:16\n\rTehnologija izrade:7 nm\nTDP:65W",
    image: "https://img.gigatron.rs/img/products/large/image5d285ab4ee185.png",
    stars: [5,5], rating: 5, dateCreated: "2021-04-11 22:00:00", isActive: "Da", madeIn: "Taiwan", quantity: 1
    },
    { id: "f153960dd788f2a6", name: "GIGABYTE nVidia GT710", category: "gpu", price: 5500.0, 
    description: "GPU:Nvidia GeForce GT 710\nKoličina memorije:1GB\nTip memorije:GDDR5\nMagistrala memorije:64bit",
    image: "https://img.gigatron.rs/img/products/large/image5ec5354af386a.png",
    stars: [3,3,4], rating: 3, dateCreated: "2020-08-19 19:30:00", isActive: "Da", madeIn: "Taiwan", quantity: 2
    },
    { id: "2d2f153960dd788f2a7", name: "MSI GEFORCE GT 1030", category: "gpu", price: 4500.0, 
    description: "GPU:Nvidia GeForce GT 1030\r\nKoličina memorije:2GB\r\nTip memorije:GDDR4\r\nMagistrala memorije:64bit",
    image: "https://img.gigatron.rs/img/products/large/image5ba37db35fe58.png",
    stars: [], rating: 0, dateCreated: "2020-08-19 19:30:00", isActive: "Ne", madeIn: "Taiwan", quantity: 0
    },
    { id: "6e02d2f153960dd788f221", name: "MSI MPG X570 GAMING", category: "mb", price: 24999.0, 
    description: "Gaming:Da\r\nTip procesora:AMD\r\nPodnožje:AMD® AM4\r\nČipset:AMD® X570\r\nFormat ploče:ATX\r\nPodržana memorija:DDR4",
    image: "https://img.gigatron.rs/img/products/large/image5d7b8ec451b7d.png",
    stars: [5,5,4], rating: 5, dateCreated: "2021-06-10 20:31:46", isActive: "Da", madeIn: "Taiwan", quantity: 5
    },
    { id: "153960dd788f221aa123", name: "ASUS TUF B550-PLUS", category: "mb", price: 22999.0, 
    description: "Gaming:Da\r\nTip procesora:AMD\r\nPodnožje:AMD® AM4\r\nČipset:AMD® B550\r\nFormat ploče:ATX\r\nPodržana memorija:DDR4",
    image: "https://img.gigatron.rs/img/products/large/image5ef47cb013d3d.png",
    stars: [5,4,3], rating: 4, dateCreated: "2021-06-08 17:31:46", isActive: "Da", madeIn: "Taiwan", quantity: 3
    },
    { id: "123123adde53453", name: "ASUS PRIME B550-PLUS", category: "mb", price: 20999.0, 
    description: "Gaming:Da\r\nTip procesora:AMD\r\nPodnožje:AMD® AM4\r\nČipset:AMD® B550\r\nFormat ploče:ATX\r\nPodržana memorija:DDR4",
    image: "https://img.gigatron.rs/img/products/large/image5d7b8ec451b7d.png",
    stars: [5], rating: 5, dateCreated: "2021-06-09 19:31:46", isActive: "Da", madeIn: "Taiwan", quantity: 5
    },
    { id: "5343123adde53453", name: "HyperX Predator 16GB", category: "ram", price: 14999.0, 
    description: "Kapacitet:16GB\r\nTip:DDR4\r\nMaksimalna frekvencija:3600MHz\r\nLatencija:CL17",
    image: "https://img.gigatron.rs/img/products/large/image5f8fe13728833.png",
    stars: [5], rating: 5, dateCreated: "2021-06-09 19:31:46", isActive: "Da", madeIn: "China", quantity: 20
    },
    { id: "143123adde53453", name: "HyperX Predator 64GB", category: "ram", price: 52999.0, 
    description: "Kapacitet:32GB x2\r\nTip:DDR4\r\nMaksimalna frekvencija:3200MHz\r\nLatencija:CL16",
    image: "https://img.gigatron.rs/img/products/large/image5f8ef3da2855c.png",
    stars: [5], rating: 5, dateCreated: "2021-06-09 19:31:46", isActive: "Da", madeIn: "China", quantity: 20
    },
    { id: "3343123adde53453", name: "HyperX Predator 16GB", category: "ram", price: 25999.0, 
    description: "Kapacitet:32GB\r\nTip:DDR4\r\nMaksimalna frekvencija:3466MHz\r\nLatencija:CL17",
    image: "https://img.gigatron.rs/img/products/large/image5f90339444d0e.png",
    stars: [5], rating: 5, dateCreated: "2021-06-09 19:31:46", isActive: "Da", madeIn: "China", quantity: 20
    },
  ];

  

  private comments : Comment[] = [
    {itemId: "60be02d2f153960dd788f2a9", username: "test", content: "Ovo je prvi komentar za ovaj proizvod.",
      postedAt: new Date("2021-06-12T22:00:00Z")
    },
    {itemId: "60be02d2f153960dd788f2a9", username: "test", content: "Ovo je drugi komentar za ovaj proizvod.",
      postedAt: new Date("2021-06-12T23:00:00Z")
    }
  ];

  constructor(private router:Router) { }

  public findAll() {
    return this.items;
  }

  public findAllByName(search : string){
    return this.items.filter(item => item.name.toUpperCase().includes(search.toUpperCase()));
  }

  public findItemById(id: string){
    return this.items.find(item => item.id == id);
  }

  public findCommentsByItemId(id: string){
    return this.comments.filter(item => item.itemId == id);
  }

  public insertComment(model: Comment){
    console.log("PUSH COMMENT: ", model);
    return this.comments.push(model);
  }

  public updateQuantity(model: Item){
    let item : Item = this.items.find(item => item.id == model.id)
    if(item){
      if(model.quantity <= item.quantity){
        item.quantity = model.quantity;
        if(item.quantity == 0){
          item.isActive = "Ne"
        }
      }
      return item;
    } else {
      return null;
    }
  }

  public cartResetQuantity(model: QuantityDeleteCart){
    return this.items.filter(item => item.id == model.id).map(item => {
      item.isActive = "Da";
      item.quantity += model.quantity;
    });
  }

  public updateStars(model: UpdatingStars){
    console.log("Update star id=", model.id, " rating=", model.rating);
    this.items.filter(item => item.id == model.id).map(item => {
        <number[]>item.stars.push(model.rating); 
        item.rating = (<number[]>item.stars).reduce((score, star) => score + star) / (<number[]>item.stars).length;
      });
    console.log("ALL: ", this.items);
  }

  public cancelOrderBackQuantity(items: any){
    return items.array.forEach(element => {
      this.items.filter(item => item.id == element.id).map(item =>{
        item.isActive = "Da";
        item.quantity += element.quantity;
      });
    });
  }

  showItem(id: String): any {
    this.router.navigate(['products/item/' + id]);
  }

}
