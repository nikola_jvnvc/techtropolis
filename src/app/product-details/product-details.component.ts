import { Component, OnInit } from '@angular/core';
import { Item, Products } from '../services/products.service.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private productsService: Products, private route:ActivatedRoute, private _snackBar: MatSnackBar, private router: Router) { }

  id: string = "";
  data: any;
  comments: any;
  currentRate: number;
  cartNumber: string;  
  username: string;

  ngOnInit(): void {
    this.cartNumber =localStorage.getItem("cartNumber");
    this.username  = localStorage.getItem("username");
    this.route.params.subscribe(value => { this.id = value["id"] });
    this.findProductsById(this.id);
    this.findAllCommentsByProductId(this.id);
  }

  onSubmit(form: NgForm){
    if (localStorage.getItem("logedin") == "true"){

      var dbQuantity = this.data.quantity
      this.data.quantity = dbQuantity - form.value.quantity;
      if (this.data.quantity < 0){

        this.data.quantity = dbQuantity;

        this._snackBar.open("Nema na stanju","",{duration: 3000});

      }else{

        this.data.quantity = form.value.quantity;
        var cartnumber: number = +localStorage.getItem("cartNumber");
        var incrementCartNumber = cartnumber++;

        if (++incrementCartNumber == parseInt(localStorage.key(parseInt(localStorage.getItem("product" + incrementCartNumber))).substring(7))){
          
          localStorage.setItem("cartNumber", ""+incrementCartNumber);
          localStorage.setItem("product" + ++incrementCartNumber,  JSON.stringify(this.data));
        }else{
          localStorage.setItem("cartNumber", ""+incrementCartNumber);
          localStorage.setItem("product" + incrementCartNumber,  JSON.stringify(this.data));
        }

        this.data.quantity = dbQuantity - form.value.quantity;

        this.updateQunatity(this.data);

        this._snackBar.open("Dodato u korpu","",{duration: 3000});

        this.cartNumber =localStorage.getItem("cartNumber");

        this.findProductsById(this.id);
      }
    }else{
      this.router.navigate(['/login'])
    }

  }

  public findProductsById(id: string): any {
    let item : Item = this.data =this.productsService.findItemById(id);
    this.currentRate = this.data.rating;
  }

  public findAllCommentsByProductId(id: string): any {
    this.comments = this.productsService.findCommentsByItemId(id);
    console.log("COMMENTS: ", this.comments);
    return
  }

  public updateQunatity(model: Item): any {
    if(this.productsService.updateQuantity(model) == null){
      this._snackBar.open("Neuspešno dodavanje u korpu!","",{duration: 3000});
    }
    return
  }

}
