import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Users, UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  message: any;
  errorMessage: string;

  constructor(private users_s: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){
    var model: Users = {
      "fullname": form.value.fullname,
      "email": form.value.email,
      "username": form.value.username,
      "password": form.value.password,
      "city": form.value.city,
      "address": form.value.address,
      "birthday": form.value.birthday,
      "picture": "../../assets/default-avatar.png"
    }
    this.message = this.users_s.insert(model);
    if(this.message == undefined){
      this.errorMessage = "Greška! E-mail ili lozinka nisu ispravni, ili je korisničko ime već zauzeto!";
    }else{
      this.router.navigate(['/login']);
    }
  }

}
