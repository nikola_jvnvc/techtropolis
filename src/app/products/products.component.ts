import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Products } from '../services/products.service.service';
import { sortBy } from 'sort-by-typescript';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private productService: Products, private router: Router ) { }

  data: any;
  dataCpy: any;
  p: number = 1;
  value: any = 0;
  cartNumber: string;  
  rating: any = 0;
  category: any = "all";
  username: string;

  ngOnInit(): void {
    this.cartNumber = localStorage.getItem("cartNumber");
    this.username = localStorage.getItem("username");
    this.findAllItems();
  }

  search(search: any){

    if (search.value == ""){
      this.findAllItems();
    } else{
      this.data = this.productService.findAllByName(search.value.trim());
    }
  }

  resetFilter() : void {
    this.category = "";
    this.value = "";
    this.rating = 0;
    this.findAllItems();
  }

  onPriceChange(value: any){

    this.value = value.value;

    if (value.value > 0){
      this.data = this.dataCpy.filter(item => item.price <= value.value)
      this.data = this.checkCategoryFilter(this.data);
      this.data = this.checkRatingFilter(this.data);
    } else if (value.value == 0){
      this.data = this.dataCpy;
      this.data = this.checkCategoryFilter(this.data);
      this.data = this.checkRatingFilter(this.data);
    }
  }

  checkPriceChange(data : any) : any {
    if (this.value > 0){
      return data.filter(item => item.price <= this.value);
    } else {
      return data;
    }
  }

  onCategoryChange(category: any){

    this.category = category.value;

    switch(category.value){
      case "all":
        this.data = this.dataCpy;
        this.data = this.checkPriceChange(this.data);
        this.data = this.checkRatingFilter(this.data);
        break;
      default:
        this.data = this.dataCpy.filter(item => item.category === category.value);
        this.data = this.checkPriceChange(this.data);
        this.data = this.checkRatingFilter(this.data);
    }

  }

  checkCategoryFilter(data : any) : any {
    switch(this.category){
      case "all":
        return data;
      default:
        return data.filter(item => item.category === this.category);
    }
  }

  onRatingChange(rating: any){
    
    this.rating = rating.value;

    if(this.rating){
      if(this.rating != 0){
        this.data = this.dataCpy.filter(item => item.rating == rating.value);
        this.data = this.checkCategoryFilter(this.data);
        this.data = this.checkPriceChange(this.data);
      } else {
        this.data = this.dataCpy;
        this.data = this.checkCategoryFilter(this.data);
        this.data = this.checkPriceChange(this.data);
      }
    } else {
      this.data = this.dataCpy;
      this.data = this.checkCategoryFilter(this.data);
      this.data = this.checkPriceChange(this.data);
    }
  }

  checkRatingFilter(data : any) : any {
    console.log("RATING: ", this.rating)
    if(this.rating){
      if(this.rating == 0)
        return data;
      else
        return data.filter(item => item.rating === this.rating);
    }
    return data;
  }

  onSortChange(sortType: any){

    switch(sortType.value){
      case "name-asc": this.data = this.data.sort(sortBy("name")); break;
      case "name-dsc": this.data = this.data.sort(sortBy("-name")); break;
      case "price-asc": this.data = this.data.sort(sortBy("price")); break;
      case "price-dsc": this.data = this.data.sort(sortBy("-price")); break;
      case "date-asc": this.data = this.data.sort(sortBy("dateCreated")); break;
      case "date-dsc": this.data = this.data.sort(sortBy("-dateCreated")); break;
      default: this.data = this.dataCpy;
    }
  }

  public findAllItems(): any {
    this.data = this.productService.findAll();
    this.dataCpy = this.data; 
  }

  public showOneItem(id: String): any {
    this.productService.showItem(id);
  }

  get ratingsValue():string{
    return this.rating > 0 ? this.rating : "Sve";
  }

  get valuesValue():string{
    return this.value > 0 ? this.value : "-";
  }

}
